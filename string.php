<?php

/**
 * Liste des fonctions string
 *
 * PHP Version 8.1
 *
 * @category  Documentation
 * @package   NhuHoaiRobertVo\StudyGuidePhpZend
 * @author    Nhu-Hoai Robert Vo <nhuhoai.vo@nhuvo.ch>
 * @copyright 2022 Nhu-Hoai Robert Vo
 * @license   MIT License https://opensource.org/licenses/MIT
 * @version   GIT: 1.0.0
 * @link      https://www.php.net/manual/en/ref.strings.php
 * @since     1.0.0
 */

namespace NhuVo\StudyGuidePhpZend\String;

/**
 * Ajoute un backslash pour chaque caractère représenter dans $characters
 *
 * @param string $string     String à modifier
 * @param string $characters Liste de caractères où il faut ajouter un backslash
 *
 * @return string
 */
function addcslashes(
    string $string,
    string $characters
): string {
  return "";
}

/**
 * Ajoute un backslash à tous les caractères où il devrait en avoir
 * Les caractères concernées sont: ', ", \ et NUL (NUL byte, soit \0)
 *
 * @param string $string String à modifier
 *
 * @return string
 */
function addslashes(
    string $string
): string {
  return "";
}

/**
 * Convertit des données binaire dans une représentation hexadécimale
 *
 * @param string $string Un string
 *
 * @return string
 */
function bin2hex(
    string $string
): string {
  return "";
}

// chop => rtrim

/**
 * Genère un string sur un byte à partir d'un codepoint
 *
 * @param int $codepoint Entier entre 0 et 255
 *
 * @return string Un caractère sur un byte
 */
function chr(
    int $codepoint
): string {
  return "";
}

/**
 * Sépare un string en plusieurs blocs
 *
 * @param string $string    String à traiter
 * @param int    $length    Longeur des blocs
 * @param string $separator Séparateur entre chaque bloc
 *
 * @return string
 */
function chunk_split(
    string $string,
    int $length = 76,
    string $separator = "\r\n"
): string {
  return "";
}

/**
 * Décode un string uuencoded
 * UUencode permet de convertir des données binaire en code ASCII grâce au
 * format UNIX UUencode
 *
 * @param string $string Données converties UUEncode
 *
 * @return string|false Données décodées ou false si échec
 */
function convert_uudecode(
    string $string
): string {
  return "";
}

/**
 * Encode des données binaires au format UUEncode
 * UUencode permet de convertir des données binaire en code ASCII grâce au
 * format UNIX UUencode
 *
 * @param string $string Données binaires à convertir
 *
 * @return string|false Retourne le string encodé, si $string vide, alors retourne false
 */
function convert_uuencode(
    string $string
): string {
  return "";
}

/**
 * Retourne des informations concernant les caractères utilisées
 *
 * @param string $string String à examiner
 * @param int    $mode   - 0: tableau avec byte-valeur comme clé et la fréquene de chaque byte comme valeur
 *                       - 1: Comme 0 mais uniquement des fréquences de byte plus grands que 0
 *                       - 2: Comme 0 mais uniquement des fréquences de byte égal à 0
 *                       - 3: string contenant tous les caractères unique
 *                       - 4: string contenant tous les caractères non utilisées
 *
 * @return array|string
 */
function count_chars(
    string $string,
    int $mode = 0
): array {
  return [];
}

/**
 * Calcule l'empreinte CRC32 d'un string
 *
 * @param string $string String à travailler
 *
 * @return int
 */
function crc32(
    string $string
): int {
  return 0;
}

/**
 * Hash un string
 *
 * @param string $string String à hasher
 * @param string $salt   Sel pour le hashage
 *
 * @return string
 */
function crypt(
    string $string,
    string $salt
): string {
  return "";
}

/**
 * Split un string dans un tableau avec un séparateur défini
 *
 * @param string $separator Séparateur
 * @param string $string    String à splitter
 * @param int    $limit     Nombre d'éléments max dans le tableau de retour.
 *                          Si $limite < 0, tous les éléments sauf les $limite dernières éléments sont retournés
 *                          Si $limite == 0, alors PHP considère que c'est égal à 1
 *
 * @return array
 */
function explode(
    string $separator,
    string $string,
    int $limit = PHP_INT_MAX
): array {
  return [];
}

/**
 * Ecrit dans un stream un string formatté
 *
 * @param resource $stream    Resource de fichier
 * @param string   $format    Format du texte
 * @param mixed    ...$values Liste des valeurs pour le texte formatté
 *
 * @return int Nombre de bytes écrits dans le stream
 */
function fprintf(
    $stream,
    string $format,
    ...$values
): int {
  return 0;
}

/**
 * Retourne la table des conversions pour htmlentities et htmlspecialchars
 *
 * @param int    $table    Quelle table faut-il retourner
 *                         HTML_ENTITIES ou HTML_SPECIALCHARS
 * @param int    $flags    Que doit contenir la table (bitwise)
 *                         - ENT_COMPAT: double-quotes mais pas de single-quotes
 *                         - ENT_QUOTES: doubles quotes et single-quotes
 *                         - ENT_NOQUOTES: pas de double quotes ni single quote
 *                         - ENT_SUBSTITUTE: remplace les code invalide par U+FFFD (ou &#xFFFD)
 *                         - ENT_HTML401: Table pour HTML 4.01
 *                         - ENT_XML1: Table pour HTML 1
 *                         - ENT_XHTML: Table pour XHTML
 *                         - ENT_HTML5: Table pour HTML 5
 * @param string $encoding Encodate à uniliser
 *                         - ISO-8859-1
 *                         - UTF-8
 *                         - cp1252 (Windows-1252)
 *
 * @return array
 */
function get_html_translation_table(
    int $table = HTML_SPECIALCHARS,
    int $flags = ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401,
    string $encoding = "UTF-8"
): array {
  return [];
}

/**
 * Converti un texte logique hébreux vers un texte visuel
 *
 * @param string $string             String en hébreux
 * @param int    $max_chars_per_line Indique le nombre maximum de caractère par ligne
 *
 * @return string
 */
function hebrev(
    string $string,
    int $max_chars_per_line = 0
): string {
  return "";
}

/**
 * Converti une chaîne hexadécimal en binaire
 *
 * @param string $string Chaîne en hexadécimal
 *
 * @return string
 */
function hex2bin(
    string $string
): string {
  return "";
}

/**
 * Convertit les entités HTML à leur caractères correspondant
 *
 * @param string $string   Texte à convertir
 * @param int    $flags    Voir get_html_translation_table
 * @param string $encoding Voir get_html_translation_table
 *
 * @return string
 */
function html_entity_decode(
    string $string,
    int $flags = ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401,
    ?string $encoding = null
): string {
  return "";
}

/**
 * Convertit tous les caractères éligiles en entitées HTML
 *
 * @param string $string        Texte à convertir
 * @param int    $flags         Voir get_html_translation_table
 * @param string $encoding      Voir get_html_translation_table
 * @param bool   $double_encode Si faux, alors PHP n'encode pas les entitées HTML
 *
 * @return string
 */
function htmlentities(
    string $string,
    int $flags = ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401,
    ?string $encoding = null,
    bool $double_encode = true
): string {
  return "";
}

/**
 * Décode les entités HTML spéciales en caractères
 *
 * @param string $string Texte à convertir
 * @param int    $flags  Voir get_html_translation_table
 *
 * @return string
 */
function htmlspecialchars_decode(
    string $string,
    int $flags = ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401
): string {
  return "";
}
/**
 * Convertit tous les caractères spéciaux en entité HTML
 *
 * @param string $string        Texte à convertir
 * @param int    $flags         Voir get_html_translation_table
 * @param string $encoding      Voir get_html_translation_table
 * @param bool   $double_encode Si faux, alors PHP n'encode pas les entitées HTML
 *
 * @return string
 */
function htmlspecialchars(
    string $string,
    int $flags = ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401,
    ?string $encoding = null,
    bool $double_encode = true
): string {
  return "";
}

/**
 * Rassemble les éléments d'un tableau dans un string
 *
 * @param string $separator Séparateur à ajouter entre chaque élément
 * @param array  $array     Tableau à convertir en string
 *
 * @return string
 */
function implode(
    string $separator,
    array $array
): string {
  return "";
}

// join => implode

/**
 * Met le premier caractère en minuscule
 *
 * @param string $string Texte à modifier
 *
 * @return string
 */
function lcfirst(
    string $string
): string {
  return "";
}

/**
 * Calcule la distance entre deux chaînes
 *
 * @param string $string1          Texte 1
 * @param string $string2          Texte 2
 * @param int    $insertion_cost   Poids lors d'un ajout
 * @param int    $replacement_cost Poids lors d'un changement
 * @param int    $deletion_cost    Poids lors d'une suppression
 *
 * @return int
 */
function levenshtein(
    string $string1,
    string $string2,
    int $insertion_cost = 1,
    int $replacement_cost = 1,
    int $deletion_cost = 1
): int {
  return 0;
}

/**
 * Lit la configuration locale
 *
 * @return array
 */
function localeconv(): array
{
  return [];
}

/**
 * Supprime les espaces (ou autres caractères) de début de chaîne
 *
 * @param string $string     Texte
 * @param string $characters Liste des caractères à supprimer
 *
 * @return string
 */
function ltrim(
    string $string,
    string $characters = " \n\r\t\v\x00"
): string {
  return "";
}

/**
 * Calcule le md5 d'un fichier
 *
 * @param string $filename Chemin d'accès au fichier à traiter
 * @param bool   $binary   Si vrai, retourne le prétraitement en format binaire brut avec une grandeur de 16
 *
 * @return string
 */
function md5_file(
    string $filename,
    bool $binary = false
): string {
  return "";
}

/**
 * Calcule le md5 d'un string
 *
 * @param string $string String à traiter
 * @param bool   $binary Si vrai, retourne le prétraitement en format binaire brut avec une grandeur de 16
 *
 * @return string
 */
function md5(
    string $string,
    bool $binary = false
): string {
  return "";
}

/**
 * Calcule cla clé metaphone du string
 *
 * @param string $string       Texte à analyser
 * @param int    $max_phonemes Restreint la clé métaphone retournée
 *
 * @return string
 */
function metaphone(
    string $string,
    int $max_phonemes = 0
): string {
  return "";
}

/**
 * Retourne un nombre au format monétaire
 *
 * @param string $format Format de la monnaie
 * @param float  $number Nombre à convertir
 *
 * @return string
 */
function money_format(
    string $format,
    float $number
): string {
  return "";
}

/**
 * Rassemble des informations sur la langue et la configuration locale
 *
 * @param int $item ID de l'élément souhaité (il y a une liste de constante)
 *                  - DAY_1 pour le nom du jour de la semaine
 *                  - CURRENCY_SYMBOL pour le symbole monétaire
 *
 * @return string
 */
function nl_langinfo(
    int $item
): string {
  return "";
}

/**
 * Insère un retour à la ligne HTML à chaque nouvelle ligne
 *
 * @param string $string    Texte à modifier
 * @param bool   $use_xhtml Si vrai, ajoute <br /> si faux ajoute <br>
 *
 * @return string
 */
function nl2br(
    string $string,
    bool $use_xhtml = true
): string {
  return "";
}

/**
 * Format un nombre
 *
 * @param float  $num                 Nombre à convertir
 * @param int    $decimals            Décimale max autorisé
 * @param string $decimal_separator   Caractère de séparation pour les décimales
 * @param string $thousands_separator Caractère de séparation pour les milliers
 *
 * @return string
 */
function number_format(
    float $num,
    int $decimals = 0,
    ?string $decimal_separator = ".",
    ?string $thousands_separator = ","
): string {
  return "";
}

/**
 * Convertit le premier octet d'une chaîne en valeur entre 0 et 255
 *
 * @param string $character String
 *
 * @return int
 */
function ord(
    string $character
): int {
  return 0;
}

/**
 * Analyse une chaîne de caractères d'une variable (représentation URL de paramètres)
 *
 * @param string $string String à analyser
 * @param array  $array  Résultat
 *
 * @return void
 */
function parse_str(
    string $string,
    array &$array
): void {
}

/**
 * Affiche un string formaté
 *
 * @param string $format    Formattage
 * @param mixed  ...$values Valeurs utilisées pour le formattage
 *
 * @return string
 */
function printf(
    string $format,
    ...$values
): int {
  return 0;
}

/**
 * Convertit une chaîne quoted-printable en chaîne de 8 bits
 *
 * @param string $string String à convertir
 *
 * @return string
 */
function quoted_printable_decode(
    string $string
): string {
  return "";
}

/**
 * Convertit une chaîne de 8 bits en chaîne quoted-printable
 *
 * @param string $string String à convertir
 *
 * @return string
 */
function quoted_printable_encode(
    string $string
): string {
  return "";
}

/**
 * Protège les métacaractères
 * Ajoute un backslash pour les caractères suivants: . \ + * ? [ ^ ] ( $ )
 *
 * @param string $string String à protéger
 *
 * @return string
 */
function quotemeta(
    string $string
): string {
  return "";
}

/**
 * Supprime les espaces (ou autres caractères) de fin de chaîne
 *
 * @param string $string     Texte
 * @param string $characters Liste des caractères à supprimer
 *
 * @return string
 */
function rtrim(
    string $string,
    string $characters = " \n\r\t\v\x00"
): string {
  return "";
}

/**
 * Modifie les informations de localisation
 *
 * @param int    $category Catégorie de fonctions affectées
 * @param string $locales  Si vide, alors c'est les locales de l'environnement
 *                         Si 0, aucune modification et c'est la localisation courante qui est retournée
 * @param string ...$rest  Options de chaînes à essayer jusqu'à la réussite
 *
 * @return string|false
 */
function setlocale(
    int $category,
    string $locales,
    string ...$rest
): string {
  return "";
}

/**
 * Calcule le sha1 d'un fichier
 *
 * @param string $filename Chemin d'accès au fichier à traiter
 * @param bool   $binary   Si vrai, retourne le prétraitement en format binaire brut avec une grandeur de 16
 *
 * @return string
 */
function sha1_file(
    string $filename,
    bool $binary = false
): string {
  return "";
}

/**
 * Calcule le sha1 d'un string
 *
 * @param string $string String à traiter
 * @param bool   $binary Si vrai, retourne le prétraitement en format binaire brut avec une grandeur de 16
 *
 * @return string
 */
function sha1(
    string $string,
    bool $binary = false
): string {
  return "";
}

/**
 * Calcule la similitude entre deux string
 *
 * @param string $string1 Texte 1
 * @param string $string2 Texte 2
 * @param float  $percent Pourcentage de similitude
 *
 * @return int
 */
function similar_text(
    string $string1,
    string $string2,
    float &$percent = null
): int {
  return 0;
}

/**
 * Calcule la clé soundex
 *
 * @param string $string String à analyser
 *
 * @return string
 */
function soundex(
    string $string
): string {
  return "";
}

/**
 * Retourne un string formaté
 *
 * @param string $format    Format du string
 * @param mixed  ...$values Valeurs pour le formatage du string
 *
 * @return string
 */
function sprintf(
    string $format,
    ...$values
): string {
  return "";
}

/**
 * Analyse un string à l'aide d'un format
 *
 * @param string $string  String à analyser
 * @param string $format  Format
 * @param mixed  ...$vars Variables pour l'analyse du formatage
 *
 * @return array|int|null Si $vars est vide, les valeurs retrouvées seront retournées sous forme de tableau.
 *                        Si le $vars comporte des données, la fonction retourne le nombre de valeurs assignées.
 *                        S'il y a plus de sous-chaînes attendus dans $format qu'il y a de disponible dans $string
 *                        alors null sera retourné
 */
function sscanf(
    string $string,
    string $format,
    &...$vars
) {
}

/**
 * Détermine si un string contient le sous string recherché
 *
 * @param string $haystack Le string dans lequel on fait la recherche
 * @param string $needle   Le sous-string à trouver
 *
 * @return bool
 */
function str_contains(
    string $haystack,
    string $needle
): bool {
  return true;
}

/**
 * Détermine si un string finit bien par un sous-string défini
 *
 * @param string $haystack Le string dans lequel on fait la recherche
 * @param string $needle   Le sous-string à trouver
 *
 * @return bool
 */
function str_ends_with(
    string $haystack,
    string $needle
): bool {
  return true;
}

/**
 * Analyse une chaîne de caractère CSV dans un tableau
 *
 * @param string $string    String à analyser
 * @param string $separator Caractère de séparateur de colonne
 * @param string $enclosure Caractère d'enclosure pour un texte
 * @param string $escape    Caractère d'échappement
 *
 * @return array
 */
function str_getcsv(
    string $string,
    string $separator = ",",
    string $enclosure = "\"",
    string $escape = "\\"
): array {
  return [];
}

/**
 * Remplace toutes les occurences dans une chaîne de manière insensitive
 *
 * @param array|string $search  Eléments à faire remplacer
 * @param array|string $replace Eléments de remplacement
 * @param string|array $subject Sujet de la recherche
 * @param int          $count   Nombre d'éléments remplacés
 *
 * @return string|array Si $subject est un string, alors on retourne un string sinon array
 */
function str_ireplace(
    $search,
    $replace,
    $subject,
    int &$count = null
): string {
  return "";
}

/**
 * Effectue un rotation 13 sur les caractères
 * C'est à décalage de 13 dans l'alphabet, les autres caractères sont inchangés
 *
 * @param string $string String à modifier
 *
 * @return string
 */
function str_rot13(string $string): string
{
}

/**
 * Mélange les caractères d'une chaîne
 *
 * @param string $string String à mélanger
 *
 * @return string
 */
function str_shuffle(string $string): string
{
}

/**
 * Convertit une chaîne en tableau
 *
 * @param string $string String à convertir
 * @param int    $length Longueur maximum de chaque élément
 *
 * @return array
 */
function str_split(
    string $string,
    int $length = 1
): array {
  return [];
}

/**
 * Contrôle que le string commence bien par le sous-string défini
 *
 * @param string $haystack Le string dans lequel on fait la recherche
 * @param string $needle   Le sous-string à trouver
 *
 * @return bool
 */
function str_starts_with(
    string $haystack,
    string $needle
): bool {
  return true;
}

/**
 * Compte le nombre de mots utiliss dans une chaîne
 *
 * @param string  $string     String à compter
 * @param int     $format     - 0: retourne le nombre de mots trouvés
 *                            - 1: retourne un tableau contenant tous les mots trouvés
 *                            - 2: retourune un tableau associatif, où la clé indique la position
 *                            numérique du mo à l'intérieur de string et la valeur est le mot actuel
 * @param ?string $characters Liste de caractères additionnels qui seront considérés comme un mot
 *
 * @return array
 */
function str_word_count(
    string $string,
    int $format = 0,
    ?string $characters = null
): array {
  return [];
}

/**
 * Comparaison insensible à la casse de chaînes
 *
 * @param string $string1 Texte 1
 * @param string $string2 Texte 2
 *
 * @return int
 */
function strcasecmp(
    string $string1,
    string $string2
): int {
  return 0;
}

// strchr --> strtr

/**
 * Comparaison de chaîne agvec l'algorithme d'ordre natuel (insensible à la casse)
 * Ordre naturel serait l'exemple de ces fichiers: img1.png, img10.pgn, img12.pgn et img2.png:
 * img1.png, img2.png, img10.png, img12.png
 *
 * @param string $string1 Texte 1
 * @param string $string2 Texte 2
 *
 * @return int
 */
function strnatcasecmp(
    string $string1,
    string $string2
): int {
  return 0;
}


/**
 * Comparaison de chaîne agvec l'algorithme d'ordre natuel (sensible à la casse)
 * Ordre naturel serait l'exemple de ces fichiers: img1.png, img10.pgn, img12.pgn et img2.png:
 * img1.png, img2.png, img10.png, img12.png
 *
 * @param string $string1 Texte 1
 * @param string $string2 Texte 2
 *
 * @return int
 */
function strnatcmp(
    string $string1,
    string $string2
): int {
  return 0;
}

/**
 * Comme strcasecmp mais on peut définit les n premiers éléments à contrôler
 *
 * @param string $string1 Texte 1
 * @param string $string2 Texte 2
 * @param int    $length  Longueur des chaînes à utiliser
 *
 * @return int
 */
function strncasecmp(
    string $string1,
    string $string2,
    int $length
): int {
  return 0;
}

/**
 * Comme strcmp mais on peut définit les n premiers éléments à contrôler
 *
 * @param string $string1 Texte 1
 * @param string $string2 Texte 2
 * @param int    $length  Longueur des chaînes à utiliser
 *
 * @return int
 */
function strncmp(
    string $string1,
    string $string2,
    int $length
): int {
  return 0;
}

/**
 * Recherche un ensemble de caractères dans une chaîne
 *
 * @param string $string     String à analyser
 * @param string $characters Caractères à rechercher
 *
 * @return string|false Affiche le reste du string dès que le premier caractère est trouvé (y compris)
 */
function strpbrk(
    string $string,
    string $characters
): string {
  return "";
}

/**
 * Retourne la position de la première occurence trouvée
 *
 * @param string $haystack Texte à analyiser
 * @param string $needle   Sous string à trouver
 * @param int    $offset   On décale le début de la recherche
 *
 * @return int|false
 */
function strpos(
    string $haystack,
    string $needle,
    int $offset = 0
): int {
  return 0;
}

/**
 * Trouve la dernière occurence d'un caractère dans une chaîne
 *
 * @param string $haystack Texte à analyser
 * @param string $needle   Caractère à chercher (si plusieurs, alors le premier est pris en compte uniquement)
 *
 * @return string|false Retourne la fin de la chaîne depuis la dernière occurrence trouvée
 */
function strrchr(
    string $haystack,
    string $needle
): string {
  return "";
}

/**
 * Inverse une chaîne
 *
 * @param string $string Texte à inverser
 *
 * @return string
 */
function strrev(string $string): string
{
}

/**
 * Retourne la position de la dernière occurence trouvée (insensitive)
 *
 * @param string $haystack Texte à analyiser
 * @param string $needle   Sous string à trouver
 * @param int    $offset   On décale le début de la recherche
 *
 * @return int|false
 */
function strripos(
    string $haystack,
    string $needle,
    int $offset = 0
): int {
  return 0;
}

/**
 * Retourne la position de la dernière occurence trouvée (sensitive)
 *
 * @param string $haystack Texte à analyiser
 * @param string $needle   Sous string à trouver
 * @param int    $offset   On décale le début de la recherche
 *
 * @return int|false
 */
function strrpos(
    string $haystack,
    string $needle,
    int $offset = 0
): int {
  return 0;
}

/**
 * Trouve la longueur du segment initial d'une chaîne contenant tous les caractères d'une masque donné
 *
 * @param string $string     Texte à analyser
 * @param string $characters Liste des caractères à chercher
 * @param int    $offset     Décalage pour la recherche
 * @param ?int   $length     Longueur max pour l'analyse
 *
 * @return int
 */
function strspn(
    string $string,
    string $characters,
    int $offset = 0,
    ?int $length = null
): int {
  return 0;
}
