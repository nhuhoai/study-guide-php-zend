# PHP Study Guide

## Ouverture PHP dans un fichier HTML 

| Ouverture                     | Fermeture       | Note           |
| ----------------------------- | --------------- | -------------- |
| ```<?php```                   | ```?>```        |                |
| ```<?=```                     | ```?>```        |                |
| ```<?```                      | ```?>```        | Obsolète       |
| ```<script language="php">``` | ```</script>``` | Ne pas utilisé |
| ```<%```                      | ```%>```        | Obsolète       |

```php
// Ceci
<?= $variable ?>

// Equivaut à cela
<?php echo $variable ?>
```

<hr />

## Opérations logiques avec les balises PHP

```html
Montant: 
<?php if ($montant >= 0): ?>
<p class="black">
<?php else: ?>
<p class="red">
<?php endif; ?>
<?= $montant ?>
</p>
```

<hr />

## Constructeurs de language

-   Ce ne sont pas des fonctions (et donc pas un ```Callable```)
-   S'il y a un paramètre, il n'est pas nécessaire de le mettre en parenthèse
-   S'il y a plusieurs paramètres, alors il ne faut JAMAIS mettre de parenthèse (en fait, c'est ok pour ```echo``` mais il faut les parenthères pour ```print``` ou ```list```)

```php
echo "Hello\n"; // OK
echo ("World\n"); // OK
echo "Hello", " ", "World!"; // OK
// echo ("Hello", " ", "World!"); // Erreur de syntaxe
```

Liste des contructeurs de language

-   ```assert```, commande de debug pour un test avec condition
-   ```echo```, imprime un contenu à la sortie ```stdout```

    Toutes les fonctions et constructeurs de language retourne une valeur ne serait-ce ```null``` (pour les ```void```). Sauf pour ```echo``` qui retourne vraiment rien du tout.
    ```php
    // $valeur = echo "Hello world!"; // Retoure une erreur de syntaxe
    ```
-   ```print```, imprime un contenu à la sortie ```stdout```
-   ```exit```, arrête le script (peut afficher un message en option)
    ```php
    exit; // Quitte le script
    exit(); // Quitte le script
    exit("Mon message d'arrêt"); // Affiche le message et quitte le script
    exit(1); // Arrête le script (n'affiche pas le 1) mais la valeur peut être récupérée dans la 
             // console comme indicateur d'arrêt. Par défaut, c'est 0 ce qui signifie que le 
             // script s'est arrêté correctement. Les valeurs autorisées sont 0 à 254, 255 est 
             // réservé à PHP
    ```
-   ```die```, alias de ```exit```
-   ```return```, termine une fonction, si appelé depuis le scope global, alors arrête le script
-   ```include```, inclue un fichier PHP et l'évalue. Un avertissement apparaît si le fichier est introuvable ou n'est pas lisible
-   ```incude_once```, comme ```include``` mais inclue le fichier PHP uniquement une fois
-   ```require```, comme ```include``` mais si le fichier n'existe pas ou n'est pas lisible, alors PHP provoque une erreur fatale
-   ```require_once```, comme ```require``` mais inclue le ficheir PHP uniquement une fois
-   ```eval```, PHP évalue un string et le traite comme script
    ```php
    eval("echo 'Hello world';"); // Affiche Hello world
    // eval("echo 'Hi everyone!'"); // Erreur de syntaxe, pas de fermeture ;
    $valeur = eval("echo 'Hello world';"); // Retourne null tant qu'il n'y a pas de return
    $valeur = eval("echo 'Hello world'; return 1;"); // Retourne 1
    ```
-   ```empty```, retourne un booléan pour indiquer si une variable est vide ou pas. Une variable vide est soit pas déclarée, soit équivalente à l'une des valeurs suivantes:
    -   ```null```
    -   ```0```
    -   ```0.0```
    -   ```"0"```
    -   ```false```
    -   ```[]```
-   ```isset```, retourne un booléan pour insiquer si la variable a été défini et qu'elle ne soit pas ```null```
-   ```unset```, supprime une variable
-   ```list```, assigne plusieurs variables à partir d'un tableau
    ```php
    list($zero, $un, $deux) = [0, 1, 2]; // $zero = 0; $un = 1; $deux = 2;
    list($zero, $un) = [0, 1, 2]; // $zero = 0; $un = 1;
    list($zero, $un, $deux) = [0, 1]; // Avertissement undefined array key 2.
                                      // $zero = 0; $un = 1; $deux = null (même si elle avait une
                                      // valeur avant)
    ```

### Remarque sur ```include_once``` et ```require_once```

Il n'est pas conseillé d'utilisé ces deux fonctions car cela ralentit l'exécution du script, PHP doit à chaque fois parcourir les fichiers inclus

## Commentaires

```php
# Perl style
// C style
/*
Multi lignes
*/
```

### Documentation PHP

Il existe une syntaxe pour la documentation mais c'est une convention et non une règle.

Pour plus de détails, se renseigner avec les programmes de génération de documentation tel PHPDocumentor ou Doxygen.

```php
/**
 * Documentation
 */
```

## Représentation des entiers

Par défaut, PHP traite tous les nombres entiers en base 10 mais il est possible de les saisir depuis une autre base telle que:

| Nom         | Base | Préfixe              |
| ----------- | ---- | -------------------- |
| Décimal     | 10   |                      |
| Binaire     | 2    | ```0b``` ou ```0B``` |
| Octal       | 8    | ```0```              |
| Hexadécimal | 16   | ```0x``` ou ```0X``` |

```php
$decimal = 1234;      echo $binaire . PHP_EOL;     // 1324
$binaire = 0b1001;    echo $binaire . PHP_EOL;     // 9
$octal = 012;         echo $octal . PHP_EOL;       // 10
$hexadecimal = 0x1A2; echo $hexadecimal . PHP_EOL; // 418
```

### Notation des valeurs hexadécimales

Les valeurs hexadécimales peuvent être annotées avec des minuscules comme des majuscules.

## Représentation des nombres réels

```php
$decimal = 123.456;       print $decimal;     // 123.456
$exponential = 0.12345e3; print $exponential; // 123.456
$exponential = 0.12345E3; print $exponential; // 123.456
```

PHP affiche si possible en valeur avec une virgule, mais à partir de ```e-5``` alors PHP affichera en exponentiel. Pour un entier qui atteint ```e+19```, c'est identique;

```php
echo 0.00001; // 1.0E-5
echo 10000000000000000000; // 1.0+19
```

## Types de variables

Il existe 2 types de variables:

-   Scalaire
    | Type    | Alias                      | Contenu                                  |
    | ------- | -------------------------- | ---------------------------------------- |
    | Boolean | ```boolean```, ```bool```  | ```true``` ou ```false```                |
    | Integer | ```integer```, ```int```   | Nombre entier signé                      |
    | Double  | ```double```, ```float```  | Nombre réel signé                        |
    | String  | ```string```               | Collection ordonnées de données binaires |
-   Composite
    -   Tableaux
    -   Objets

## Casting de variables

### Entier

```php
$a = 0; var_dump($a);  // int(0)

var_dump((double) $a); // double(0)
var_dump((bool) $a);   // bool(false)
var_dump((string) $a); // string(1) "0"
var_dump((array) $a);  // array(1) {[0] => int(0)}

$a = 2; var_dump($a);  // int(2)
var_dump((double) $a); // double(2)
var_dump((bool) $a);   // bool(true)
var_dump((string) $a); // string(1) "2"
var_dump((array) $a);  // array(1) {[0] => int(2)}

$a = -3; var_dump($a); // int(-3)
var_dump((double) $a); // double(-3)
var_dump((bool) $a);   // bool(true)
var_dump((string) $a); // string(2) "-3"
var_dump((array) $a);  // array(1) {[0] => int(-3)}
```

### Nombre réel

```php
$a = 0.0; var_dump($a);   // double(0)
var_dump((int) $a);       // int(0)
var_dump((bool) $a);      // bool(false)
var_dump((string) $a);    // string(1) "0"
var_dump((array) $a);     // array(1) {[0] => double(0)}

$a = 1.01; var_dump($a);  // double(1.01)
var_dump((int) $a);       // int(1)
var_dump((bool) $a);      // bool(true)
var_dump((string) $a);    // string(4) "1.01"
var_dump((array) $a);     // array(1) {[0] => double(1.01)}

$a = -2.34; var_dump($a); // double(-2.34)
var_dump((int) $a);       // int(-2)
var_dump((bool) $a);      // bool(true)
var_dump((string) $a);    // string(5) "-2.34"
var_dump((array) $a);     // array(1) {[0] => double(-2.34)}
```

### Booléan

```php
$a = false; var_dump($a); // bool(false)
var_dump((int) $a);       // int(0)
var_dump((double) $a);    // double(0)
var_dump((string) $a);    // string(0) ""
var_dump((array) $a);     // array(1) {[0] => bool(false)}

$a = true; var_dump($a);  // bool(true)
var_dump((int) $a);       // int(1)
var_dump((double) $a);    // double(1)
var_dump((string) $a);    // string(1) "1"
var_dump((array) $a);     // array(1) {[0] => bool(true)}
```

### Chaîne de caractères

```php
$a = ""; var_dump($a);            // string(0) ""
var_dump((int) $a);               // int(0)
var_dump((double) $a);            // double(0)
var_dump((bool) $a);              // bool(false)
var_dump((array) $a);             // array(1) {[0] => string(0) ""}

$a = "Hello world"; var_dump($a); // string(11) "Hello world"
var_dump((int) $a);               // int(0)
var_dump((double) $a);            // double(0)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(11) "Hello world"}

$a = "false"; var_dump($a);       // string(5) "false"
var_dump((int) $a);               // int(0)
var_dump((double) $a);            // double(0)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(5) "false"}

$a = "Hi 007"; var_dump($a);      // string(6) "Hi 007"
var_dump((int) $a);               // int(0)
var_dump((double) $a);            // double(0)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(6) "Hi 007"}

$a = "12345"; var_dump($a);       // string(5) "12345"
var_dump((int) $a);               // int(12345)
var_dump((double) $a);            // double(12345)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(5) "12345"}

$a = "0"; var_dump($a);           // string(1) "0"
var_dump((int) $a);               // int(0)
var_dump((double) $a);            // double(0)
var_dump((bool) $a);              // bool(false)
var_dump((array) $a);             // array(1) {[0] => string(1) "0"}

$a = "-200"; var_dump($a);        // string(4) "-200"
var_dump((int) $a);               // int(-200)
var_dump((double) $a);            // double(-200)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(4) "-200"}

$a = "1.234"; var_dump($a);       // string(5) "1.234"
var_dump((int) $a);               // int(1)
var_dump((double) $a);            // double(-1.234)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(5) "1.234"}

$a = "0.0"; var_dump($a);         // string(3) "0.0"
var_dump((int) $a);               // int(0)
var_dump((double) $a);            // double(0)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(3) "0.0"}

$a = "-9.876"; var_dump($a);      // string(6) "-9.876"
var_dump((int) $a);               // int(-9)
var_dump((double) $a);            // double(-9.876)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(6) "-9.876"}

$a = "1.11E2"; var_dump($a);      // string(6) "1.11E2"
var_dump((int) $a);               // int(111)
var_dump((double) $a);            // double(111)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(6) "1.11E2"}

$a = "1.23E-20"; var_dump($a);    // string(8) "1.23E-20"
var_dump((int) $a);               // int(0)
var_dump((double) $a);            // double(1.23E-20)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(8) "1.23E-20"}

$a = "null"; var_dump($a);        // string(4) "null"
var_dump((int) $a);               // int(0)
var_dump((double) $a);            // double(0)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(4) "null"}

$a = "\n"; var_dump($a);          // string(1) "\n"
var_dump((int) $a);               // int(0)
var_dump((double) $a);            // double(0)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(1) "\n"}

$a = "\0"; var_dump($a);          // string(1) "\000"
var_dump((int) $a);               // int(0)
var_dump((double) $a);            // double(0)
var_dump((bool) $a);              // bool(true)
var_dump((array) $a);             // array(1) {[0] => string(1) "\000"}
```

### Tableaux

```php
$a = []; var_dump($a);        // array(0) {}
var_dump((int) $a);           // int(0)
var_dump((double) $a);        // double(0)
var_dump((bool) $a);          // bool(false)
var_dump((string) $a);        // Avertissement array to string conversion
                              // string(5) "Array"

$a = [0]; var_dump($a);       // array(1) {[0] => int(0)}
var_dump((int) $a);           // int(1)
var_dump((double) $a);        // double(1)
var_dump((bool) $a);          // bool(true)
var_dump((string) $a);        // Avertissement array to string conversion
                              // string(5) "Array"

$a = [0, 1, 2]; var_dump($a); // array(3) {[0] => int(0) [1] => int(1) [2] => int(2)}
var_dump((int) $a);           // int(1)
var_dump((double) $a);        // double(1)
var_dump((bool) $a);          // bool(true)
var_dump((string) $a);        // Avertissement array to string conversion
                              // string(5) "Array"

$a = [[], []]; var_dump($a);  // array(2) {[0] => array(0) {} [1] => array(0) {}}
var_dump((int) $a);           // int(1)
var_dump((double) $a);        // double(1)
var_dump((bool) $a);          // bool(true)
var_dump((string) $a);        // Avertissement array to string conversion
                              // string(5) "Array"
```

### Valeur ```null```

```php
$a = null; var_dump($a); // NULL
var_dump((int) $a);      // int(0)
var_dump((double) $a);   // double(0)
var_dump((bool) $a);     // bool(false)
var_dump((string) $a);   // string(0) ""
var_dump((array) $a);    // array(0) {}
```

### Danger avec l'utilisation des nombres réels

Le stockage d'un nombre entier est strict en binaire. Il n'y a pas de perte de conversion entre la base 10 et la base 2. Ce n'est pas le cas de la réprésentation décimale et binaire pour les nombres réels. Par exemple, ```0.8``` en décimal est un nombre fini alors que la représentation binaire représente un nombre non fini ```0b001111110100110011001100[1100...]```. À cause de cela, il y a une perte d'information lors d'une reconversion en entier.

```php
echo (int) ((0.1 + 0.5) * 10); // 6, OK
echo (int) ((0.1 + 0.7) * 10); // 7, car 0.8 en binaire, c'est comme 0.79999....
```

Il ne faut pas se fier sur la précision aussi des nombres réels. Mieux vaux effectuer un calcul en tenant compte du degré de précision.

```php
$pi = 3.14159625;
$indiana = 3.2;
$precision = 0.00001;

// Valeurs non identiques
echo (abs($pi - $indiana) < $precision) ? "Valeurs identique" : "Valeurs non identique"; 
```

## Nom des variables

Règles sur les noms de variables

-   Les noms sont sensitives
-   Les noms comportent uniquement des lettres, des chiffres et le caractère ```_``` (underscore)
-   Les noms ne peuvent pas commencer par un chiffre

Par convention, il est préférable d'utiliser les styles suivants pour nommer les noms des variables:

-   camelCase
-   StudlyCase
-   snake_case

## Variable de variable de nom

```php
// Variable de variable de nom standard
$a = "foo";  // $a vaut "foo"
$$a = "bar"; // Comme $a vaut "foo" donc $foo vaut "bar"
echo $foo;   // bar

// Variable de variable de nom avec un tableau
$a = "foo";
$$a["bar"] = "Hello world";
var_dump($foo); // array(1) {"bar" => string(11) "Hello world"}
var_dump($$a);  // array(1) {"bar" => string(11) "Hello world"}
```

## Espaces de noms

Il est possible de déclarer de plusieurs manière des espaces de noms:

-   Format avec le ```;```
    ```php
    // Hormis declare, on ne peut rien déclarer avant la déclaration d'un namespace sinon cela
    // provoque une erreure fatale: namespace declaration statement has to be the very first statement
    // or after any declare call
    declare(strict_types=1);
    // $a = 0;
    // const VALEUR = "GLOBALE";
    // session_start();

    // Attention, on ne peut rien déclarer avant un namespace
    // Espace de nom A
    namespace A;

    const VALEUR = "A";

    // Espace de nom B
    namespace B;

    const VALEUR = "B";

    echo \A\VALEUR . PHP_EOL; // A
    echo VALEUR . PHP_EOL;    // B

    // Il est possible d'avoir des sous-espaces
    namespace B\C;

    const VALEUR = "C";
    echo \A\VALEUR . PHP_EOL; // A
    echo \B\VALEUR . PHP_EOL; // B
    echo VALEUR . PHP_EOL;    // C

    // Il n'est pas possible de revenir à l'espace global sous cette écriture
    // Mais il est possible d'accéder aux fonctions et constantes de l'espace globale

    echo \DIRECTORY_SEPARATOR . PHP_EOL; // '/' ou '\' selon le système d'exploitation
    ```
-   Format avec enclosure ```{}```
    ```php
    // Hormis declare, on ne peut rien déclarer avant la déclaration d'un namespace sinon cela
    // provoque une erreure fatale: namespace declaration statement has to be the very first statement
    // or after any declare call
    declare(strict_types=1);
    // $a = 0;
    // const VALEUR = "GLOBALE";
    // session_start();

    // Espace de noms global
    namespace {
      const VALEUR = "GLOBAL";
    }

    // Attention, on ne peut rien déclarer avant un namespace
    // Espace de nom A
    namespace A {
      const VALEUR = "A";
      echo VALEUR . PHP_EOL;  // A
      echo \VALEUR . PHP_EOL; // GLOBAL
    }

    // Espace de nom B
    namespace B {
      const VALEUR = "B";

      echo \A\VALEUR . PHP_EOL; // A
      echo VALEUR . PHP_EOL;    // B
    }

    // Il est possible d'avoir des sous-espaces
    namespace B\C {
      const VALEUR = "C";
      echo \A\VALEUR . PHP_EOL; // A
      echo \B\VALEUR . PHP_EOL; // B
      echo VALEUR . PHP_EOL;    // C
      echo \VALEUR . PHP_EOL;   // GLOBAL
    }

    // Retour à la namespace globale
    namespace {
      echo VALEUR . PHP_EOL; // GLOBAL
    }
    ```

### Raccourcis dans les espaces de noms

Il est possible d'utiliser ```use``` pour raccourcir les appels de classes ou de fonctions d'un autre espace de nom.

```php
namespace A {
  class Test { }
}

namespace B {
  use A\Test;
  use Exception;

  // Avec use, cela nous évite de récrire l'espace de nom pour Test
  $a = new Test;
  
  // Use peut être utilisé sur des classes globales
  class CustomException extends Exception { }
}
```

## Constantes

Ce sont des variables mais elles sont inchangeables (immutable). Par convention, on les nomme tout en majuscule. Il est possible de déclarer de deux manières une constante, chacune des deux manières a un impact sur les noms d'espace.


```php
namespace A {
  define("PI", 3.142);    // Constante globale
  const REPONSE = 42;     // Constante locale au nom d'espace

  echo PI . PHP_EOL;      // 3.142
  echo REPONSE . PHP_EOL; // 42
}

namespace {
  echo PI . PHP_EOL;        // 3.142
  echo A\REPONSE . PHP_EOL; // 42
}

namespace B {
  const REPONSE = 0;     // Il est possible de redéfinir une constante locale avec un même nom 
                         // d'un autre espace de nom
  // define("PI", 3.14); // Mais il est interdit d'utiliser define, sinon cela fait une erreur
  define("B\\PI", 3.14); // Néanmoins, il est possible d'assigner directement un espace de nom
                         // dans define

  echo PI . PHP_EOL;      // 3.14 et non 3.142
  echo REPONSE . PHP_EOL; // 0
}

namespace {
  echo PI . PHP_EOL;        // 3.142
  echo B\PI . PHP_EOL;      // 3.14
  echo A\REPONSE . PHP_EOL; // 42
  echo B\REPONSE . PHP_EOL; // 0
}
```

Autres règles:

-   Il n'est pas possible d'assigner une variable à une constante avec ```const``` mais il est possible avec ```define```
    ```php
    $test = 0;
    // const A = $test; // Fatal error: Constant expression contains invalid operation
    define("A", $test); // ça marche, A restera toujours 0 même si on change $test
    ```
-   Il est possible d'utiliser des valeurs scalaires statiques
    ```php
    const A = "Hello" . " " . "world";
    const B = A . "!";
    ```

Pour contrôler l'existance d'une constante, il est possible d'utiliser la fonction ```defined``` qui retournera un booléan ou la fonction ```constant``` qui retournera la valeur de la constante ou le cas échéant lancera une ```Error```.

```php
namespace A {
  const VALA = 0;
}

namespace {
  const VALB = 1;

  var_dump(defined("VALA"));    // bool(false)
  var_dump(defined("A\\VALA")); // bool(true)
  var_dump(defined("VALB"));    // bool(true)

  try {
    echo constant("VALB") . PHP_EOL;    // 1;
    echo constant("A\\VALA") . PHP_EOL; // 0;
    echo constant("VALA") . PHP_EOL;    // Provoque une erreur
  } catch(Throwable $t) {
    var_dump($t); // Error(Undefined constant "VALA")
  }
}
```

## Constantes

| Constante                  | Type   | Description | Exemple |
| -------------------------- | ------ | ----------- | ------- |
| ```DEFAULT_INCLUDE_PATH``` | string | Si un fichier n'est pas trouvé, il sera recherché dans ces répetoires par défaut | .;/etc/php/pear |
| ```E_ALL```                | int    | Flag pour indiquer tous les messages d'erreur | 32767 |
| ```E_COMPILE_ERROR```      | int    | Flag pour indiquer une erreur du compilateur PHP | 64 |
| ```E_COMPILE_WARNING```    | int    | Flag pour indiquer un avertissement du compilateur PHP | 128 |
| ```E_CORE_ERROR```         | int    | Flag pour indiquer une erreur du noyau PHP | 16 |
| ```E_CORE_WARNING```       | int    | Flag pour indiquer un avertissement du noyau PHP | 32 |
| ```E_DEPRECATED```         | int    | Flag pour indiquer une obsolescence PHP | 8192 |
| ```E_ERROR```              | int    | Flag pour indiquer une erreur PHP | 1 |
| ```E_NOTICE```             | int    | Flag pour indiquer un remarque PHP | 8 |
| ```E_PARSE```              | int    | Flag pour indiquer une erreur de parsing PHP | 4 |
| ```E_RECOVERABLE_ERROR```  | int    | Flag pour indiquer une erreur fatale cacthable | 4096 |
| ```E_STRICT```             | int    | Flag pour indiquer une erreur interopérabilité PHP | 248 |
| ```E_USER_DEPRECATED```    | int    | Flag pour indiquer une obsolescente utilisateur | 16384 |
| ```E_USER_ERROR```         | int    | Flag pour indiquer une erreur utilisateur | 256 |
| ```E_USER_NOTICE```        | int    | Flag pour indiquer une remarque utilisateur | 1024 |
| ```E_USER_WARNING```       | int    | Flag pour indiquer un avertissement utilisateur | 512 |
| ```E_WARNING```            | int    | Flag pour indiquer un avertissement PHP | 2 |
| ```PEAR_INSTALL_DIR```     | string | Répertoire d'installation des commandes PEAR | /etc/php/pear |
| ```PEAR_EXTENSION_DIR```   | string | Répertoire des extension PEAR | /etc/php/ext |
| ```PHP_BINARY```           | string | Chemin du fichier binaire PHP | C:\php\bin\php.exe |
| ```PHP_BINDIR```           | string | Répertoire où se trouve le fichier binaire PHP | C:\php\bin |
| ```PHP_DEBUG```            | int    | Si le mode debug est actif | 1 |
| ```PHP_EOL```              | string | Caractère de fin de ligne | \n |
| ```PHP_EXTENSION_DIR```    | string | Répertoire des extension PHP | /etc/php/ext |
| ```PHP_EXTRA_VERSION```    | string | Nom complémentaire de la version | extra |
| ```PHP_FLOAT_DIG```        | int    | Décimale max avant la perte de précision pour un nombre réeél | 15 |
| ```PHP_FLOAT_EPSILON```    | double | La plus petite représentation d'un nombre positif réel | 2.2204460492503E-16 |
| ```PHP_FLOAT_MAX```        | double | Valeur max pour un nombre réel | 1.7976931348623E+308 |
| ```PHP_FLOAT_MIN```        | double | Valeur min pour un nombre réel | 2.2250738585072E-308 |
| ```PHP_INT_MAX```          | int    | Valeur max d'un entier | 9223372036854775807 |
| ```PHP_INT_MIN```          | int    | Valeur min d'un entier | -9223372036854775808 |
| ```PHP_INT_SIZE```         | int    | Nombre de bytes pour un string | 8 |
| ```PHP_MAJOR_VERSION```    | int    | Version majeure PHP | 5 |
| ```PHP_MANDIR```           | string | Répertoire où se trouvent les fichiers man |  |
| ```PHP_MAXPATHLENGTH```    | int    | Longueur maximum d'un chemin | 2048 |
| ```PHP_DEBUG```            | int    | Si le mode debug est actif | 1 |
| ```PHP_MINOR_VERSION```    | int    | Version mineure PHP | 2 |
| ```PHP_OS```               | string | Système d'exploitation utilisé | WINNT |
| ```PHP_OS_FAMILY```        | string | Famille du système d'exploitation utilisé | Windows |
| ```PHP_PREFIX```           | string | Répertoire où se trouve PHP | C:\php |
| ```PHP_RELEASE_VERSION```  | int    | Numéro de release PHP | 7 |
| ```PHP_SAPI```             | string | Server API utilisé | cli |
| ```PHP_VERSION```          | string | Version PHP au format _major.minor.release[extra]_ | 5.2.7-extra |
| ```PHP_VERSION_ID```       | int    | Version PHP sous forme d'entier | 50207 |
| ```PHP_ZTS```              | int    | Indique si la version de PHP est Zend Thread Safety | 1 |


## Constantes magiques

| Constante magique   | Description |
| ------------------- | ----------- |
| ```__LINE__```      | Retourne la ligne actuelle du script exécuté |
| ```__FILE__```      | Nom et chemin absolu du script PHP exécuté   |
| ```__CLASS__```     | Espace de noms et nom de la class du script actuellement exécuté (si héritage, sans déclaration dans l'enfant, alors c'est la classe parent qui sera affichée |
| ```__METHOD__```    | Nom de la méthode exécutée, si exécuté depuis une classe, elle retourne le nom de la classe aussi |
| ```___FUNCTION__``` | Nom de la fonction exécutée (n'affiche pas la classe parente si existante) |
| ```__TRAIT__``` | Espace de nom ainsi que le nom du trait du script exécuté |
| ```__NAMESPACE__``` | Nom de l'espace de noms actuel |

## Super globales

| Super globale   | Description |
| --------------- | ----------- |
| ```$GLOBALS```  | Tableau contenant toutes les super globales |
| ```$_SERVER```  | Données concernants l'URL, l'en-tête et autres informations relevant du serveur |
| ```$_GET```     | Données en provenance des paramètres GET dans l'URL |
| ```$_POST```    | Données en provenance des données POST |
| ```$_FILES```   | Fichiers uploader dans une requête POST |
| ```$_COOKIE```  | Données en provenance des cookies |
| ```$_SESSION``` | Données de session |
| ```$_REQUEST``` | Combinaison des plusieurs super globales, par défaut EGPSC |
| ```$_ENV```     | Variables d'environnement |

### Champs liés à $_SERVER

| Indexe                      | Description | Exemple |
| --------------------------- | ----------- | ------- |
| ```argv```                  | Tableau des arguments passé en ligne de commande | array([0] => /var/www/html/index.php [1] => -S) |
| ```argc```                  | Nombre d'arguments passé en ligne de commande | 2 |
| ```AUTH_TYPE```             | Type d'authentification |  |
| ```DOCUMENT_ROOT```         | Répertoire racine web | /var/www/html |
| ```GATEWAY_INTERFACE```     | Quel version CGI est utilisée | CGI/1.1 |
| ```HTTP_ACCEPT```           | Contenu de l'en-tête ```Accept``` | text/html |
| ```HTTP_ACCEPT_CHARSET```   | Contenu de l'en-tête ```Accept-Charset``` |  |
| ```HTTP_ACCEPT_ENCODING_``` | Contenu de l'en-tête ```Accept-Encoding``` | gzip, deflate, br |
| ```HTTP_ACCEPT_LANGUAGE```  | Contenu de l'en-tête ```Accept-Language``` | en-US,fr |
| ```HTTP_CONNECTION```       | Contenu de l'en-tête ```Connection``` | keep-alive |
| ```HTTP_HOST```             | Contenu de l'en-tête ```Host``` | localhost |
| ```HTTP_REFERER```          | Contenu de l'en-tête ```Referer``` |  |
| ```HTTP_USER_AGENT```       | Contenu de l'en-tête ```User-Agent``` | Mozilla/5.0 |
| ```HTTPS```                 | Contenu non vide si protocole ```https``` | 1 |
| ```PATH_INFO```             | Chemin relatif au fichier sans les paramètres | /test/index.php |
| ```PATH_TRANSLATED```       | Chemin du fichier exécuté après le remaping virtuel |  |
| ```PHP_AUTH_DIGEST```       | Contenu de l'en-tête ```Authorization``` lors d'une authentification Digest HTTP |  |
| ```PHP_AUTH_USER```       | Utilisateur lors d'une authentification HTTP |  |
| ```PHP_AUTH_PW```       | Mot de passe lors d'une authentification HTTP |  |
| ```PHP_SELF```              | Nom et chemin absolu du fichier PHP exécuté | /var/www/html/index.php |
| ```QUERY_STRING```          | Query string (partie GET) | objet=velo&montant=1000.15 |
| ```REDIRECT_REMOVE_USER```  | Redirection interne si utilisateur authentifié | /home |
| ```REMOTE_ADDR```           | Adresse IP du client | 54.251.2.9 |
| ```REMOTE_HOST```           | Nom de l'hôte vu par le client à travers un reverse dns lookup | 125.98.56.32 |
| ```REMOTE_PORT```           | Numéro du port d'écoute du client | 58948 |
| ```REMOTE_USER```           | Utilisateur authentifié |  |
| ```REQUEST_METHOD```        | Méthode de la requête | POST |
| ```REQUEST_SCHEME```        | Schéma de la requête (http/https) | http |
| ```REQUEST_TIME```          | Timestamp de la requête | 1648558543 |
| ```REQUEST_TIME_FLOAT```    | Timestamp de la requête | 1648558543.6203 |
| ```REQUEST_URI_```          | URI d'appel au script avec les paramètres | /test/index.php?valeur=1 |
| ```SCRIPT_FILENAME```       | Nom et chemin absolu du fichier PHP exécuté | /var/www/html/index.php |
| ```SCRIPT_NAME```           | Chemin au script actuel sans le domaine | /test/index.php |
| ```SERVER_ADDR```           | Adresse IP du serveur web | 127.0.0.1 |
| ```SERVER_ADMIN```          | Nom de l'admin indiqué dans la config serveur | info@apache.org |
| ```SERVER_NAME```           | Nom du serveur web | localhost |
| ```SERVER_PORT```           | Port d'écoute du serveur | 80 |
| ```SERVER_PROTOCOL```       | Version du protocole de communication | HTTP/1.1 |
| ```SERVER_SIGNATURE```      | Version du serveur et du virtual host actuel | Apache/2.4.52 Server at localhost Port 9000 |
| ```SERVER_SOFTWARE```       | Nom du serveur web | Apache/2.4.52 (Win64) |

## Opérateurs arithmétiques

| Opération      | Opérateur | Exemple        |
| -------------- | --------- | -------------- |
| Addition       | ```+```   | ```1 + 2.3```  |
| Soustaction    | ```-```   | ```4 - 5```    |
| Division       | ```/```   | ```6 / 7```    |
| Multiplication | ```*```   | ```8 * 9```    |
| Modulo         | ```%```   | ```10 % 11```  |
| Puissance      | ```**```  | ```12 ** 13``` |

Pour la puissance, il est possible d'utiliser la fonction ```pow```.

## Affectations arithmétiques

| Commande         | Sortie  | Valeur $a après<br /> commande | Description |
| ---------------- | ------- | ------------------------------ | ----------- |
| ```$a = 1;```    | ``` ``` | ```1```                        |             |
| ```echo $a++;``` | ```1``` | ```2```                        | Postfix     |
| ```echo ++$a;``` | ```3``` | ```3```                        | Prefix      |
| ```echo $a--;``` | ```3``` | ```2```                        | Postfix     |
| ```echo --$a;``` | ```1``` | ```1```                        | Prefix      |

## Opérateurs logiques

| Opération   | Opérateur | Exemple         |
| ----------- | --------- | --------------- |
| Et          | ```and``` | ```$a and $b``` |
| Et          | ```&&```  | ```$a && $b```  |
| Ou          | ```or```  | ```$a or $b```  |
| Ou          | ```||```  | ```$a || $b```  |
| Ou exclusif | ```xor``` | ```$a xor $b``` |
| Ou exclusif | ```^```   | ```$a ^ $b```   |
| Pas         | ```!```   | ```!$a```       |

Notice sur les priorités des opérateurs: ```and```, ```or``` et ```xor``` sont moins prioritaires que ```=```!

```php
$a = true;
$b = false;
$low = $a and $b;
$high = $a && $b;
var_dump($low);  // bool(true)
var_dump($high); // bool(false)
```

## Opérateur ternaire

```php
var_dump(true ? "True" : "False");  // string(4) "True"
var_dump(false ? "True" : "False"); // string(5) "False"
var_dump(true ? : "False");         // bool(true)
// var_dump(false ? "False");       // Pas possible, erreur de syntaxe
```

## Opérateurs de coalescence null et Elvis

```php
// Version longue
$a = isset($_GET["a"]) ? $_GET["a"] : "ASC";

// Avec opérateur de coalescence null
$a = $_GET["a"] ?? "ASC";

// Opérateurs de coalescence null peuvent être chaînés
$a = $_GET["a"] ?? $_POST["a"] ?? $_COOKIE["a"] ?? "ASC";

// Opérateur Elvis remonte une remarque si $_GET n'est pas attribué
$a = $_GET["a"] ?: "ASC";
```

## Opérateur "vaisseau spacial" (spaceship)

```php
echo (string) (0 <=> 1) . PHP_EOL; // -1
echo (string) (0 <=> 0) . PHP_EOL; // 0
echo (string) (1 <=> 0) . PHP_EOL; // 1

echo (string) ("a" <=> "b") . PHP_EOL; // -1, a < b
echo (string) ("a" <=> "B") . PHP_EOL; // 1, en ASCII:  B < a
```

## Opérateurs binaires (bitwise)

Pour les commandes suivantes prendre la situation initiale suivante:

```php
$a = 0b110010; // 50
$b = 0b011001; // 25
// En fait, l'annotation binaire officiel est sous 32 bits (ou 64 selon la version PHP compilée)
// $b vaut en faut 0b00000000000000000000000000011001
```

| Opération         | Opérateur | Commande       | Binaire                            | Décimal |
| ----------------- | --------- | -------------- | ---------------------------------- | ------- |
| Et                | ```&```   | ```$a & $b```  | 0b010000                           | 16      |
| Ou                | ```\|```  | ```$a \| $b``` | 0b111011                           | 59      |
| Ou exclusif       | ```^```   | ```$a ^ $b```  | 0b101011                           | 43      |
| Inverse           | ```~```   | ```~$a```      | 0b11111111111111111111111111001101 | -51     |
| Décalage à droite | ```>>```  | ```$a >> 2```  | 0b001100                           | 12      |
| Décalage à gauche | ```<<```  | ```$a << 3```  | 0b11001000                         | 400     |

Remarque pour les décalages à gauche, si on dépasse le nombre de bits de la version PHP compilée, on obtient 0 dû à un dépassement de mémoire.

```php
var_dump(1 << 2); // int(4)
var_dump(1 << 8); // int(256)
var_dump(1 << 32); // int(0) sur un système 32-bit ou int(4294967296)
var_dump(1 << 64); // int(0) sur un système 32-bit ou 64-bit
var_dump(1 << 80); // int(0) sur un système 32-bit ou 64-bit
```

## Opérateurs d'assignement

```php
var_dump($a = 0);   // int(0)
var_dump($a += 10); // int(10)
var_dump($a -= 5);  // int(5)
var_dump($a *= 4);  // int(20)
var_dump($a /= 2);  // int(10)
var_dump($a %= 4);  // int(2)
var_dump($a **= 3); // int(8)
var_dump($a |= 1);  // int(9)  = 0b00001001 = 0b00001000 | 0b00000001
var_dump($a &= 19); // int(1)  = 0b00000001 = 0b00001001 & 0b00010011
var_dump($a ^= 15); // int(14) = 0b00001110 = 0b00000001 ^ 0b00001111
var_dump($a >>= 2); // int(3)  = 0b00000011
var_dump($a <<= 2); // int(12) = 0b00001100
```

## Opérateur de référence

L'opérateur de référence, ou un assignement par référence, est représenté par le caractère ```&```.

```php
$a = 1;
$b = &$a;
$b++;
echo $a; // 2;
```

Cet opérateur est utilisable uniquement sur des variables. À noter que PHP assignera toujours une instance d'objet par référence et qu'il ne faut pas le faire explicitement à la déclaration d'une instance.

```php
// $a = &new stdClass; // Parse error: syntax error, unexpected 'new'
// $a = new &stdClass; // Parse error: syntax error, unexpected token "&"
```

## Opérateurs de comparaison

| Opérateur | Description                               |
| --------- | ----------------------------------------- |
| ```>```   | Plus grand que                            |
| ```>=```  | Plus grand ou égal à                      |
| ```<```   | Plus petit que                            |
| ```<=```  | Plus petit ou égal à                      |
| ```<>```  | Différent de                              |
| ```==```  | Equivalent (on ne contrôle pas le typage) |
| ```===``` | Identique (on contrôle le typage aussi)   |
| ```!=```  | N'est pas équivalent                      |
| ```!==``` | N'est pas identique                       |

### Comparaison ente deux tableaux

Pour ```==``` entre deux tableaux:

-   Pas de contrôle de typage pour les clés ni les valeurs
-   Les clés n'ont pas besoin d'être ordonnées

Pour ```===``` entre deux tableaux:

-   Contrôle de typage des valeurs
-   Pas de contrôle de typage des clés
-   Les clés doivent être ordonnées

Pour ```>```, ```>=```, ```<```, ```<=``` ou ```<=>``` entre deux tableaux:

-   C'est le nombre de clé qui va être utilisé comme poids de comparaison

```php
$a = [0, 1, 2];
$b = [0, "1", 2];
$c = ["0" => 0, 1 => 1, 2 => 2];
$d = ["2" => 2, "1" => 1, "0" => 0];

var_dump($a == $b);     // bool(true)
var_dump($a == $c);     // bool(true)
var_dump($a == $d);     // bool(true)
var_dump($a === $b);    // bool(false)
var_dump($a === $c);    // bool(true)
var_dump($a === $d);    // bool(false)

var_dump([0] > [1, 2]); // bool(true)
```

### Comparaison entre un tableau et une variable scalaire

Le tableau est toujours plus grand d'une variable scalaire

```php
var_dump([] == 1);          // bool(false)
var_dump([] > PHP_INT_MAX); // bool(true)
var_dump([] > "123");       // bool(true)
```

## Opérateur pour cacher les avertissements

Pour ignorer et cacher les messages PHP (sauf exeption et erreur fatale), il est possible d'utiliser ```@``` précédent la fonction pouvant provoquer des erreurs.

```php
@include "fake.php"; // N'affichera rien si le fichier est introuvable
```

## Opérateur pour commande console

Pour exécuter une commande console comme si on éxécutait la fonction ```shell_exec```, il est possible d'entourer la commande par les reverses quotes ``` ` ```.

```php
echo `whoami`; // Retourne le nom d'utilisateur actif du serveur lançant le script PHP
```

## Structures conditionnelles

PHP supporte les structures suivantes: ```if```, ```else```, ```elseif``` et ```switch```.

```php
// Une condition pour une instruction
if (true) {
  // Instruction si condition vraie
}

// Une condition pour deux instructions
if (true) {
  // Instruction si condition vraie
} else {
  // Instruction si condition fausse
}

// Multiple conditions avec if et else
if (true) {
  // Instruction si condition ci-dessus vraie
} else if(true) { // Annotation else if
  // Instruction si condition ci-dessus vraie
} elseif (true) { // En PHP, il est possible d'avoir l'annotation elseif
                  // mais pas elif comme bash ou python
  // Instruction si condition ci-dessus vraie
} else { // Cette partie est facultative
  // Instruction si aucune condition est vraie
}

// Multiple condition avec switch
switch ($a) {
  case 0:
    // Instruction si $a == 0
    break; // Pour indiquer la sortir du switch
  case 1:
  case 2:
  case 3:
    // Instruction si $a == 1 ou $a == 2 ou $a == 3
    break;
  default: // c'est facultatif
    // Instruction si aucune condition est vraie

    // Par convention, on met le default en dernière position mais il est
    // possible d'ajouter d'autre case après
    
    // Pour la dernière condition ou pour le default pour autant qu'il soit
    // à la fin, il n'est pas obligé de terminer avec break mais par
    // convention et sécurité, il vaut mieux l'ajouter
}
```

À noter que si un ```case``` est vrai et qu'il n'est pas clôturé par un ```break```, alors les cases suivants sont considérés comme vrai et PHP exécutera toutes les prochains instructions jusqu'au prochain ```break``` détecté.

```php
$a = 2;

switch ($a) {
  case 0:
    print "A";
    break;
  case 1:
    print "B";
    break;
  case 2:
    print "C";
  case 3:
    print "D";
  case 4:
    print "E";
    break;
  case 5:
    print "F";
    break;
  default:
    print "?";
}

// Le script va afficher CDE
```

## Boucles

PHP supporte les boucles suivantes: ```for```, ```foreach``` et ```while```.

```php
// Boucle for
for ($i = 0; $i < 10; $i++) {
  // Instruction
}

// Boucle foreach pour les itérations
foreach ([0, 1, 2] as $valeur) {
  // Instruction avec $valeur représentant la valeur de chaque élément de
  // l'itération (0, 1 et 2 dans l'exemple)
}

// Boucle foreach pour itération avec récupération de clé
foreach (["a", "b", "c"] as $cle => $valeur) {
  // Instruction avec la paire de variables $cle pour la clé de l'itération
  // en cours et $valeur pour l'élement de l'itération en cours.
  // $cle = 0; $valeur = "a";
  // $cle = 1; $valeur = "b";
  // $cle = 2; $valeur = "c";
}

// Boucle while (contrôle de la condition à chaque étape)
while (true) {
  // Instruction
}

// Boucle do...while (Exécute l'instruction au moins une fois puis
//  contrôle la condition pour les prochaines fois)
do {
  // Instruction
} while(true);
```

Il est possible de sortir d'une boucle avec l'usage de ```break```.

Il est possible d'ignorer l'itération en cours et de passer à la suivante avec ```continue```.

```php
// Imprime les nombres pairs entre 2 et 18 compris
$a = 0;
while ($a >= 0) {
  $a++;
  if ($a % 2 == 0) {
    if ($a == 20) {
      break;
    } else {
      echo $a . PHP_EOL;
    }
  } else {
    continue;
  }
}
```

Il est possible d'ajouter un nombre après ```break``` pour indiquer le nombre de boucle à quitter (pour les boucles imbriquées). Par défaut, c'est ```1``` si on ne met rien;

```php
// Affichera 01234
while (true) {
  for ($i = 0; $i < 10; $i++) {
    if ($i == 5) {
      // Comme break vaut deux, on quitte aussi la boucle while
      break 2;
    } else {
      echo $i;
    }
  }
}
```

## Configuration de PHP

Le fichier de configuration par défaut s'appelle ```php.ini```. Par défaut il se trouve dans le même répertoire que l'exécutable PHP. Le répertoire PHP par défaut se trouve dans ```/etc/php/7.4``` sur Ubuntu par exemple ou encore ```/etc/php7/``` sur Alpine. Sur Windows, c'est l'utilisateur qui décide de l'emplacement de l'installation mais si on passe avec Xampp, il propose le chemin suivant par défaut: ```C:\xampp\php```.

Pour le cas de Windows, en général, la configuration est enregistrée dans le registre à l'emplacement suivant: ```HKEY_LOCAL_MACHINE\SOFTWARE\PHP```.

Dans le cas où le fichier ```php.ini``` n'est pas trouvé, PHP va chercher dans le fichier de configuration parmi une liste de répertoires prédéfinis. Il est possible de voir cette liste avec la fonction ```php_ini_scanned_files```.

Lorsque vous utiliser un server web (Nginx, Apache2), il faut redémarrer le serveur suite à une modification de la configuration du fichier ```php.ini``` afin que les changements soient pris en compte. À la différence que par console (mode ```cli```), il n'y a pas de service à redémarrer.

Il est possible d'utiliser des variables d'environnement dans le fichier ```php.ini``` avec l'annotation suivante:

```ini
memory_limit = ${PHP_MEMORY_LIMIT}
```

### Utilisation des fichiers ```.user.ini```

Lors de l'extension ```fpm``` est activée, PHP va charger la configuration du fichier ```php.ini``` puis va essayer de cherche le fichier ```.user.ini``` dans le répertoire racine. La configuration ```.user.ini``` écrasera la configuration de ```php.ini```. Le répertoire racine est défini par la variable ```DOCUMENT_ROOT``` ou accessible via PHP avec ```$_SERVER["DOCUMENT_ROOT"]```.

S'il existe plusieurs hôtes virtuel avec un ```DOCUMENT_ROOT``` différent pour chaque hôte, alors PHP va cherche le fichier ```.user.ini``` dans le répertoire définit par ```DOCUMENT_ROOT``` pour chaque hôte.

### Utilisation des fichiers ```.htaccess``` sur Apache2

Chaque répertoire accessible depuis le web peut contenir un fichier ```.htaccess``` pour configurer le fonctionnement de PHP et d'Apache2 pour le répertoire en question. La configuration PHP dans ```.htaccess``` écrase celle de ```php.ini```.

## Gestion de la mémoire

Il est possible de contrôler et d'optimiser la mémoire dans PHP. PHP utilise un contenaire ```zval``` pour évaluer différents éléments:

| Elément  | Description |
| -------- | ----------- |
| Value    | La valeur de la variable |
| Type     | Type de données de la variable |
| Is_ref   | Indique si la variable est une référence à un autre objet ou pas |
| Refcount | Ce compteur traque le nombre de variables pointant à ce contenaire ```zval```|

Pour utiliser pleinement de la gestion de mémoire, il faut installer l'extension ```xdebug``` (ne jamais utiliser en production) et utiliser la fonction ```xdebug_debug_zval```.

### Zval sur des variables scalaires

```php
$a = "Mon texte";
xdebug_debug_zval('a'); // PHP 8.1: a: (interned, is_ref=0)='Mon texte'
                        // PHP 7.0: a: (refcount=0, is_ref=0)='Mon texte'
$b = &$a;
xdebug_debug_zval('a'); // a: (refcount=2, is_ref=1)='Mon texte'
xdebug_debug_zval('b'); // b: (refcount=2, is_ref=1)='Mon texte'

$b = "Son texte";
xdebug_debug_zval('a'); // a: (refcount=2, is_ref=1)='Son texte'
xdebug_debug_zval('b'); // b: (refcount=2, is_ref=1)='Son texte'

unset($b);
xdebug_debug_zval('a'); // a: (refcount=1, is_ref=1)='Son texte'
xdebug_debug_zval('b'); // b: no such symbol
```

## Zval sur des variables composites

Les variables de type tableau ou classe génère des zval imbriqués.

```php
$a = ["nom" => "Bob", "age" => 42];
xdebug_debug_zval('a');
// PHP 8.1: a: (refcount=2, is_ref=0)=array ('nom' => (refcount=1, is_ref=0)='Bob', 'age' => (refcount=0, is_ref=0)=42)
// PHP 7.0: a: (refcount=1, is_ref=0)=array ('nom' => (refcount=2, is_ref=0)='Bob', 'age' => (refcount=0, is_ref=0)=42)

class A {
  public $a = "OK";
  protected $b = false;
  private $c = 0;
}

$a = new A;
xdebug_debug_zval('a');
// PHP 8.1: a: (refcount=1, is_ref=0)=class A { public $a = (refcount=0, is_ref=0)='OK'; protected $b = (refcount=0, is_ref=0)=FALSE; private $c = (refcount=0, is_ref=0)=0 }
// PHP 7.0: a: (refcount=1, is_ref=0)=class A { public $a = (refcount=2, is_ref=0)='OK'; protected $b = (refcount=0, is_ref=0)=FALSE; private $c = (refcount=0, is_ref=0)=0 }
```

### Gestion des récurrences

PHP est capable de détecter les récurences et donnera l'information.

```php
class A { public $a = null; }
$a = new A;
$a->a = $a;
xdebug_debug_zval('a');
// PHP 8.1: a: (refcount=2, is_ref=0)=class A { public $a = (refcount=0, is_ref=0)=... }
// PHP 7.0: a: (refcount=2, is_ref=0)=class A { public $a = (refcount=2, is_ref=0)=... }
```

### Libération de la mémoire

Lorsqu'une variable est déclarée, un contenaire zval est créé. Si la variable est supprimée, le contenaire continue d'exister. PHP libèrera la mémoire uniquement à la fin de la requête. Il est recommandé de ne pas exécuter de long script avec PHP.

Cependant, PHP exécute un nettoyage lorsque le buffer est plein ou lorsque l'utilisateur exécute la fonction ```gc_collect_cycles```. Il est conseillé d'utiliser cette fonction lors de longues requêtes ou de gros traitements avec une base de données.

## Cache opcode

PHP compile des séquences intermédiaire d'instructions lorsqu'il exécute les scripts PHP. Ces instructions s'appellent des opcodes (ou bytecodes). Un cache opcode permet de stocker en mémoire des opcodes et permet à PHP d'accéder à ces instructions plus rapidement.

Il faut activer l'extension ```opcache``` dans PHP pour pouvoir profiter de cette option.

## Extensions

PHP offre une multitude d'extensions afin de faciliter le développement ou d'améliorer l'exécution des scripts PHP. ```PECL``` est le répertoire des extensions PHP. Pour installer des extensions, il suffit d'installer les paquets souhaités avec Linux. Sur Windows, il faut télécharger le code source le compiler (certaines extensions comportent le fichier binaire comme pour ```xdebug```).

Après, il faut activer l'extension pour PHP dans le fichier ```php.ini``` avec la commande suivante:

```ini
extension=pdo.so  # Si l'extension par défaut n'est pas configuré pour linux
extension=pdo.dll # Si l'extension par défaut n'est pas configuré pour Windows
extension=pdo # Si l'extension par défaut a été configuré
extension=/usr/lib/php/7.0/20201212/pdo.so # Si l'extension n'est pas dans le répetoire PECL
zend_extension=xdebug # Pour les extensions Zend comme xdebug
```

Il est possible d'afficher sur une page web toutes les extensions installées avec la fonction ```phpinfo```.

Si on veut traiter spécifiquement avec une extension, il est possible d'utiliser ```get_loaded_extensions``` ou ```extension_loaded```.

```php
print_r(get_loaded_extensions()); // Retourne un tableau des extensions chargées
(extension_loaded("pdo")) ?: die("PDO n'est pas chargé"); // Affiche une erreur si PDO n'est pas chargé
```

Et par ligne de commande, il est poosible d'utiliser l'argument ```-m```.

```bash
php -m
```

## Les fonctions

Une fonction est une séquence d'instructions à exécuter.

Quelques règles concernant les fonctions:

-   les nom des fonctions sont insensitive
-   les fonctions peuvent être appelé avant la définition de cette dernière
-   une fonction ne comporte que des caractères, chiffres et le caractère ```_```
-   une fonction doit toujorus commencer par un caractère ou ```_```

```php
function abc() {}
function ABCD() {}
function efg012() {}
function _hij(){}
function __kl34_mn_56() {}
// function ABC() {} // Provoque une erreur fatale: Cannot redeclare ABC()
// function 789() {} // Provoque une erreur fatale: PHP parse error
```

## Les arguments dans les fonctions

### Avec et sans typage des arguments

Il est possible de passer des argments. Ces dernier peuvent être typés.

```php
function a($a) {} // Fonction avec un argument
function b($a, $b) {}  // Fonction avec deux arguments
function c(int $a) {} // Fonction avec un argument typé
function d(int $a, string $b) {} // Fonction avec deux arguments typés
function e($a, bool $b) {} // Fonction avec un argument non typé et l'autre oui
function f(array $a, Object $b) {} // Fonction avec des arguments composites
function g(callable $a) {} // Fonction avec un argument de type callable (callback function)
function h(iterable $a) {} // Fonction avec un argument de type itérante
class A {
  public function a(self $a) {} // Fonction avec un argument d'instance identique à la classe
}
```

### Contrôle strict du typage des arguments (et étendu)

Il est possible de rentre strict le contrôle du typage en ajouter cette ligne tout au début du fichier PHP (même avant la déclaration du namespace).

```php
declare(strict_types=1);
```

### Valeurs par défaut pour un argument

Il est possible d'ajouter des valeurs par défaut pour les arguments. À noter que qu'on déclare un argument avec une valeur par défaut, les suivantes doivent aussi comporter des valeurs par défaut.

```php
function a($a = 0) {} // $a vaut 0 si on met rien
function b(string $a = "") {} // $a vaut "" si on met rien
function c(int $a, int $b, double $c = 0.0) {} // $a et $b sont obligatoires
// function d(int $a = 0, int $b) {} // Affiche un avertissement: Optional
                                     // parameter $a declared before 
                                     // required parameter $b
```
### Arguments acceptant la valeur ```null```

```php
function a(?int $a) {}; // Il est possible de d'appeler a(null); Attention, a() retournera une erreur!
function b(?int $a = null) {}; // Il est possible de mettre null comme valeur par défaut
```

### Surcharge de fonctions

En PHP, il n'est pas possible de surcharger une fonction en le redéclarant. Pour cela, il faut créer une fonction sans argument et d'utiliser les fonctions ```func_num_args```, ```func_get_arg``` et ```func_get_args```.

```php
function a() {
  switch(func_num_args()) {
    case 0:
      echo "Pas de paramètre" . PHP_EOL;
      break;
    case 1:
      echo "1 paramètre: " . func_get_arg(0) . PHP_EOL;
      break;
    case 2:
      echo "2 paramètres: " . func_get_arg(0) . "/" . func_get_arg(1) . PHP_EOL;
      break;
    default:
      echo "Il y a trop de paramètres:" . PHP_EOL;
      print_r(func_get_args());
  }
}

a();        // Pas de paramètre
a(0);       // 1 paramètre: 0
a(1, 2);    // 2 paramètres: 1/2
a(3. 4. 5); // Il y a trop de paramètres: array(3, 4, 5)
```

### Variadics

```php
function a(bool $a, int ...$b) {
  var_dump($a);
  var_dump($b);
}

// a(); // Provoque une erreur fatale: Uncaught ArgumentCountError: Too 
        // few arguments to function a()
a(true); // bool(true) array(0) {}
a(true, 0); // bool(true) array(1) {[0] => int(0)}
a(false, 1, 2, 3); // bool(true) array(3) {[0] => int(1) [1] => int(2)
                   // [2] => int(3)}
```

### Références

```php
function a(int &$a) { $a++; }
$nombre = 0;
a($nombre);
// a(&$nombre); // Provoque une erreur fatale: syntax error
echo $nombre; // 1
```

### Variable de fonctions

Comme pour les variables, il est possible d'appeler une fonction par variable.

```php
function foo() { echo "bar"; }
$var = "foo";
$var(); // bar;
```

### Retours

Il est possible de retourner une données dans une fonction. De plus, il est possible de typer le retour.

```php
function a() { return 0; }          // Retournera 0
function b(): int { return 1; }     // Retournera 1 (doit être de type int)
function c(): ?int { return null; } // Retourne null
```

Il est possible d'annoter qu'une fonctionne n'a pas de ```return``` avec le typage ```void```, attention, la fonction retournera tout de même ```null```. Mais attention, si on met dans la fonction ```return null;```, cela provoquera une erreur.

```php
function a(): void { } // OK
// function b(): void { return null; } // Provoque une erreur fatale, void
                                       // function must not return a value
function c(): void { return; } // Mais là ça fonctionne
```

### Retour par référence

Le retour par référence permet par exemple à l'accès en direct d'une valeur en provenance d'une instance.

```php
class A {
  public $a = 42;

  public function &getA() {
    return $this->a;
  }
}
$a = new A;
$valeur = &$a->getA();

echo $valeur . PHP_EOL; // 42
$a->a = 24;
echo $valeur . PHP_EOL; // 24
```

### Accès aux variables globales

Pour cela, il faut utiliser ```global``` sur les variables concernées. Les variables globales sont accessible en direct dans le scope de la fonction. Cela signifie que si on modifie les valeurs à partir de la fonction, la variable globale est mise à jour aussi dans le scope global.

```php
$y = 2;
$z = 3;

function a(int $a) {
  global $y, $z;
  echo ($a + $y + $z) . PHP_EOL;
  $z++;
}

a(1); // 6
a(1); // 7
echo $z . PHP_EOL; // 5
```

### Lambas et closures

Lambda est une fonction anonyme stockée dans une variable. Elle peut comporter des arguments.

Closure est une fonction anonymie stockée dans une variable. Elle peut comporter des argments mais aussi peut ajouter des variables globales dans sont scope. À la différence de l'utilisation de ```global```, les variables globales sont copiées et non passées par référence.

```php
$lambda = function ($a, $b) {
  echo $a + $b;
};
echo gettype($lambda) . PHP_EOL; // Object
echo (int) is_callable($lambda) . PHP_EOL; // 1
echo get_class($lambda) . PHP_EOL; // Closure
$lambda(1, 2) . PHP_EOL; // 3

$string = "Hello world";
$closure = function() use ($string) {
  $string .= "!";
  echo $string;
};
echo gettype($closure) . PHP_EOL; // Object
echo (int) is_callable($closure) . PHP_EOL; // 1
echo get_class($closure) . PHP_EOL; // Closure
$closure() . PHP_EOL; // Hello world!
echo $string . PHP_EOL; // Hello world!
```

#### Closure: early and late binding

Par défaut, les variables globales passée dans une closure est early binding ==> la fonction traite les informations au moment de la déclaration de la closure. Mais si on passe les variables globales en référence, alors ces dernières sont late binding ==> la fonction traite les variables globales au moment de l'appel de la closure et non à la déclaration.

```php
// Early binding $a
// Late binding $b

$a = "Old A";
$b = "Old B";

$c = function () use ($a, &$b) {
  echo $a . PHP_EOL . $b . PHP_EOL;
};

$a = "New A";
$b = "New B";

$c(); // Old A New B
```

### Lié une closure à un autre scope

Pour cela, il faut utiliser la fonction ```bindTo``` de la class ```Closure```.

```php
class Animal {
  public function getClosure() {
    $variable = "Animal";
    return function () use ($variable) {
      return $this->nature . " " . $variable;
    };
  }
}

class Cat extends Animal {
  protected $nature = "Awesome";
}

class Dog extends Animal {
  protected $nature = "Friendly";
}

$cat = new Cat;
$closure = $cat->getClosure();
echo $closure(); // Awesome Animal

$closure = $closure->bindTo(new Dog);
echo $closure(); // Friendly Animal
```

### Self-executing closures

On utilise ce genre de structure pour éviter de polluer le scope global.

```php
(function () {
  echo "Self-executing anonymous function" . PHP_EOL;
  echo $definedInClosure = "Variable set" . PHP_EOL;
})();

var_dump(isset($definedInClosure)); // bool(false)
```

## Callables

Ce sont des fonctions de rappel, cela permet à des fonctions de recevoir d'autres fonctions en argument.

Voici la liste de représentation possible d'un Callable:

-   Fonction anonyme sur une ligne
    ```php
    $a = 42;
    (function () { echo "Do something" . PHP_EOL; })();
    (function () use ($a) { echo "The anwser is $a" . PHP_EOL; })();
    ```
-   Variable lambda
    ```php
    $a = function () { echo "I am lambda"; };
    ```
-   Variable closure
    ```php
    $answer = 42;
    $a = function () use ($answer) { echo "Closure $answer"; };
    ```
-   Un string représentant une fonction (PHP ou définie par l'utilisateur)
    ```php
    function a(Callable $a) { $a(); }
    function b() { echo "Call B"; }
    a("b"); // Call B
    ```
-   Un tableau contenant une instance d'un objet en premier élément et le nom de la fonction à appeler en deuxième position
    ```php
    class A { public function a() { echo "Call A"; } }
    $a = new A;
    call_user_func([$a, "a"]); // Call A
    ```
-   Un Tableau contenant le nom d'une classe en premier élément et le nom de la fonction statique en deuxième position
    ```php
    class A { public static function a() { echo "Call static A"; } }
    call_user_func(["A", "a"]); // Call static A
    ```
-   Un string contenant le nom d'une méthode statique
    ```php
    class A { public static function a() { echo "Call static A"; } }
    call_user_func("A::a"); // Call static A
    ```

Il n'est pas possible d'utiliser un contructeur de langage.

```php
// call_user_func("print", "Print message"); // Fatal error: uncaught TypeError
```

## String

### Déclarer un string

Il est possible de déclarer par deux manières un string: avec des simples quote ```'``` ou double quotes ```"```. Le premier exprime un string dans l'ensemble de sa déclaration alors que le deuxième permet j'ajouter des caractères spéciaux et d'intégrer des variables.

```php
$nom = "PHP";
echo ($text = 'Mon nom est:\t$nom') . PHP_EOL; // Mon nom est:\t$nom
echo ($text = "Mon nom est:\t$nom") . PHP_EOL; // Mon nom est:  PHP
```

### Contaner un string

Pour concatener un string, il faut utiliser ```.``` pour joindre deux valeurs.

```php
$a = "!";
$b = 0;

echo ($text = $b . $a); // 0!
echo ($text = "$b Hello" . " " . "world" . $a); // 0 Hello world!
```

### Utilisation des variables dans la déclation d'un string

Il faut impérativement utiliser le double quotes ```"```. Au plus simple, la variable peut être intégrer en direct dans le string tant que PHP peut séparer le contenu de la variable. Si on veut utiliser le caractère ```$``` accolé à du texte, il faut ajouter en préfixe  ```\```.

```php
$a = 0;
echo "Valeur = $a" . PHP_EOL;         // Valeur = 0
echo "Valeur = int($a)" . PHP_EOL;    // Valeur = int(0)
// echo "Valeur = $alapin" . PHP_EOL; // avertissement --> Valeur =
echo "Valeur = \$$a" . PHP_EOL;       // Valeur = $0
```

Il est possible d'assigner un élément de tableau (index numérique) ou d'instance. Mais cela ne s'effectue qu'à un niveau: un tableau multi-dimentionnel ou une classe imbriquée ne fonctionnera pas.

```php
class A { public $a = 0; public $b = null; }
$a = new A;
$a->b = new A;
$arr = [1, 2, "test" => 3, [4, 5]];

echo "Valeur = $a->a" . PHP_EOL;           // Valeur = 0
echo "Valeur = $arr[1]" . PHP_EOL;         // Valeur = 2
// echo "Valeur = $arr["test"]" . PHP_EOL; // Syntax error

echo $a->b->a . PHP_EOL; // 0;
// echo "Valeur = $a->b->a" . PHP_EOL; // Fatal error, object class A 
                                       //cannot convert
echo $arr[2][0]; // 4;
echo "Valeur = $arr[2][0]"; // Warning array cannot convert to string 
                            // Valeur = Array[0]
```

Pour bien isoler l'appel à une variable, il faut utiliser la syntaxe suivante: ```{$variable}```. Pour les variables simple (non tableau et instance), il est possible d'utiliser la syntaxe ```${variable}```.

```php
$v = 0;
class A { public $a = 1; public $b = null; }
$a = new A;
$a->b = new A;
$a->b->a = 2;
$arr = [[42]];

echo "A{$v}A" . PHP_EOL;            // A0A
echo "A${v}A" . PHP_EOL;            // A0A
echo "B{$a->b->a}B" . PHP_EOL;      // B2B
// echo "B${a->b->a}B" . PHP_EOL;   // Erreur fatale
echo "C{$arr[0][0]}C" . PHP_EOL;    // C42C
// echo "C${arr[0][0]}C" . PHP_EOL; // Erreur fatale
```

### Caractères de contrôle

| Séquence          | Description                                |
| ----------------- | ------------------------------------------ |
| ```\n```          | Line feed                                  |
| ```\r```          | Carriage return                            |
| ```\t```          | Tabulation                                 |
| ```\v```          | Tabulation verticale                       |
| ```\e```          | Escape                                     |
| ```\f```          | Form feed                                  |
| ```\\```          | Caractère ```\```                          |
| ```\[0-7]{1,3}``` | Affiche le caractère correspondant (octal) |
| ```\x[0-9A-Fa-f]{1,2}``` | Affiche le caractère correspondant (hex) |
| ```\u{[0-9A-Fa-f]{1,6}}``` | Affiche le caractère correspondant (hex) pour UTF-8 représentation |

```php
echo "\x7C\n";      // |
echo "\110\n";      // H
echo "\u{40}\n";    // @
echo "\u{1F418}\n"; // 🐘
```

### Heredoc et nowdoc

Il est possible de saisir une chaîne de caractère sur plusieurs lignes. Tout comme la différence ente ```'``` et ```"```, Heredoc et Nowdoc se comporte de la même manière.

```php
$a = 0;

echo <<<HEREDOC
Ceci est un texte
sur plusieurs lignes.\nValeur $a
HEREDOC;
/*
Ceci est un texte
sur plusieurs lignes.
Valeurs = 0
*/

echo <<<'NOWDOC'
Ceci est un texte
sur plusieurs lignes.\nValeur $a
NOWDOC;
/*
Ceci est un texte
sur plusieurs lignes.\nValeur $a
*/
```

Les termes ```HEREDOC``` et ```NOWDOC``` peuvent être remplacés par n'importe quel autre string.

### Caractères dans un string

Un string est un tableau de caractères. Il est donc possible d'utiliser l'annotation de type tableau pour accéder à un caractère précis.

```php
$texte = "123456789";
echo $texte[0]; // 1;
echo $texte[2]; // 3;
```

Il est possible d'attribuer un nouvel caractère via cette annotation. Si l'attribution d'un nouvel caractère utilise un index plus grand que la longueur du string, PHP complètera les caractères supplémentaires par un espace jusqu'à l'index mentionné.

```php
$texte = "12345";
$texte[0] = 0;
echo $texte . PHP_EOL; // 02345
$text[8] = 9;
echo $texte . PHP_EOL; // 02345   9
```

### L'extension ```mbstring```

Par défaut, lorsqu'on travaille avec les fonctions de string de base, PHP travaille en réalité byte par byte et non caractère par caratère. Tant que le contenu se limite à l'ASCII, il n'y a pas de problème. Mais lorsqu'on utilise des accents ou caractères spéciaux, il y a des caractères qui s'étalent sur plusieurs bytes.

C'est donc la raison de la création de l'extension mbstring qui va contrôler un string caractère par caractère et non par byte.

```php
texte = "école 🐘";
echo $texte . PHP_EOL; // école 🐘
echo strlen($texte) . PHP_EOL; // 11
echo mb_strlen($texte) . PHP_EOL; // 7

echo strlen("é") . PHP_EOL; // 2
echo mb_strlen("é") . PHP_EOL; // 1

echo strlen("🐘") . PHP_EOL; // 4
echo mb_strlen("🐘") . PHP_EOL; // 1
```

Globalement, la majorité des fonctions string comporte une équivalence avec l'extension ```mbstring```. Il faut pour cela ajouter en préfixe ```mb_```.

### Encodage

En général, on ne sait pas ce qui rentre mais on sait ce qui en sort. Par défaut, PHP travail en UTF-8.

Quant aux entrées, si on n'a pas l'information, on peut essayer de le deviner avec la fonction ```mb_detect_encoding```. Cette fonction va tester une liste d'encodage (accessible et modifiable depuis ```mb_detect_order```). Dès que l'encodage est connu, il est alors possible d'utiliser ```mb_convert_encoding``` pour retourner le contenu dans le format souhaité.

```mb_detect_encoding``` présume l'encodage, ce n'est pas une science sûre.

### Comparaison entre string

Nous avons vu auparavent que lorsqu'on compare deux string, PHP contrôle contrôle caractère par caractère les string mais il y avait un problème avec les minuscules et les majuscules (MAJUSCULE < minuscule). Ceci est dû à la représentation ASCII de ces lettre. Par exemple A = 65 et a = 97.

Pour comparer de manière plus sûre (binairement parlant), il vaut mieux utiliser la fonction ```strcmp```. Et si on ne souhaite pas tenir compte de la casse, on peut utiliser ```strcasecmp``` qui fait une conversion ```lower``` avant de faire la comparaison.

```php
$a = "PHP";
$b = "developer";
echo strcmp($a, $b) . PHP_EOL; // -20
echo strcasecmp($a, $b) . PHP_EOL; // 12
```

Il est possible d'utiliser ```strncmp``` et ```strcasencmp``` pour se limiter aux n premiers caractères.

La fonction ```similar_text``` permet de calculer la similitude entre deux string. Cela retourne le nombre de caractères identiquent entre deux string. Il est possible d'utiliser un troisième argument par référence pour avoir un pourcentage de similitude. Attention, l'ordre des deux premiers arguments peut faire changer les valeurs retournées. Cette fonction est très gourmande, alors ne pas trop abuser sur de grand contenu.

```php
$st0 = similar_text("bafoobar", "barfoo", $p0);
$st1 = similar_text("barfoo", "bafoobar", $p1);
echo "$st0 ($p0%)" . PHP_EOL; // 5 (71.428571428571%)
echo "$st1 ($p1%)" . PHP_EOL; // 3 (42.857142857143%)
```

L'algorithme de levenshtein est aussi bien. Il calcule le nombre de changement à faire pour que les deux strings soient identiques. Ici, on peut inverser l'ordre des strings dans les arguments, le résultat sera toujours identique. La fonction porte le même nom que le créateur de l'algo: ```levenshtein```.

Il est possible de comparer des sous-string avec ```substr_compare```.

Comme il est possible de comparer phonétiquement des strings avec ```metaphone``` ou ```soundex```.

Si on souhaite comparer des strings hachés, il faudrait utiliser les fonction ```hash_equals``` ou ```password_verify```.

### Extraction de strings

Utilsier la fonction ```substr```.

### Recherche dans les strings

La fonction ```strpos``` permet de retourner la position de la première occurence trouvée.

### Liste des fonctions en relation avec du string

Voir le ficher [string.php](string.php)

